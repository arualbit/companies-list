(function() {

	/**
	 * Hace una request del mock server para obtener el listado de empresas
	 * @param  {string}
	 * @param  {callback}
	 * @return  {boolean}
	 */
	var loadJSON = function(url, callback) {
		var xobj = new XMLHttpRequest();
			xobj.overrideMimeType('application/json');
		xobj.open('GET', url, true); // Replace 'my_data' with the path to your file
		xobj.onreadystatechange = function () {
			  if (xobj.readyState == 4 && xobj.status == '200') {
				callback(xobj.responseText);
			  }
		};
		xobj.send(null); 
	};
	
	/**
	 * Construye los nodos html para lista de empresas desde el listado json
	 * @param  {json}
	 */
	var buildList = function(list) {
		var listContainerElement = document.getElementById('js-list'),
			listElement = document.createElement('div'),
			fragment = document.createDocumentFragment(),
			itemHTML = '',
			listJSON = JSON.parse(list);

		for(var item in listJSON) {
			var compData = listJSON[item];
			var company = '<div><h2>'.concat(compData.company, '</h2><p>', compData.description, '</p>')
								.concat('<p>Country: ', compData.country, '</p>')
								.concat('<p><a class="c-list__link" href="mailto:', compData.email, '">Contactar</a>')
								.concat('<a class="c-list__link" href="http://www.', compData.url, '" target="_blank">Website</a></p></div>')
								.concat('<div class="l-list__button"><button class="c-list__button js-button" id="', compData.id, 
									'" data-company="', compData.company, '">Eliminar</button></div>');
			itemHTML += ''.concat('<li class="c-list__item" id="company-', compData.id, '">', company, '</li>');
		}

		itemHTML = '<ul class="c-list">'.concat(itemHTML, '</ul>');
		listElement.innerHTML = itemHTML;
		fragment.appendChild(listElement.firstChild);

		listContainerElement.appendChild(fragment);

		setButtonListener();
	}; 

	/**
	 * Añade el listener a cada boton y muestra un popup para con la confirmacion antes de eliminar la empresa
	 * 
	 */
	var setButtonListener = function() {
		var buttons = document.getElementsByClassName("js-button"),
			i = 0,
			buttonsLength = buttons.length;

		for (i; i < buttonsLength; i++) {
		    buttons[i].addEventListener('click', function(e) {
		    	var confirmAction = window.confirm('Esta seguro de eliminar la empresa ' + e.target.dataset.company + '?');
		    	if(confirmAction === true) {
		    		(elem=document.getElementById('company-' + e.target.id)).parentNode.removeChild(elem);
		    	}
		    }, false);
		}
	};
	
	loadJSON('server/listing.json', buildList);

})();