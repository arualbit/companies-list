# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Ejercicio de Javascript para un proceso de selección
* Version 1.0

### How do I get set up? ###

* Para ejecutar la prueba se necesita un servidor local para evitar los errores de cross origin en Ajax cuando hace la request para cargar el archivo JSON.
* Puede utilizar cualquier servidor local, mi sugerencia es con Node instalado hacer `npm install -g http-server` y una vez instalado ejecutar `http-server` desde el root del proyecto.
* Por favor tenga en cuenta que se utiliza window.confirm en Javascript para mostrar el popup de confirmación, si presiona el checkbox para evitar que aparezcan mas cuadros de dialogos adicionales no se podría eliminar el item de la lista. 